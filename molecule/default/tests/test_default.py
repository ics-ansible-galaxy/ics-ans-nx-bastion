import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('nx_bastion')


def test_nx(host):
    service = host.service("nxserver")
    assert service.is_running
    assert service.is_enabled


def test_csstudio_installed(host):
    assert host.file('/opt/cs-studio/ESS CS-Studio').exists
