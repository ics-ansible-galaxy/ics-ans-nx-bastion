ics-ans-nx-bastion
===================

Ansible playbook to install NoMachine Terminal Server.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
